const tagBlacklist = ["SCRIPT", "STYLE"];
const punctuation = [undefined, ' ', ' ', ' ', '.', '…', ':', ';', ',', '-', ')', '<', '>', "\n", "\r"];
const regex = /[\.·⋅](euse|e|rice|ice|le|lle|ne|nne|fe|se)([\.·⋅]?s)?/gi;
const dictionary = {
  "iel": "il",
  "ielle": "il",
  "ille": "il",
  "elleux": "eux",
  "ellui": "lui",
  "tous et toutes": "tous",
  "toutes et tous": "tous",
  "celles et ceux": "ceux"
};

function isOptionalPluralAt(text, matchEndIndex) {
  return text.substr(matchEndIndex, 3) === "(s)";
}

function replaceInclusiveWriting(text) {
  var newText = "";
  var match;
  var lastIndex = 0;

  while (match = regex.exec(text)) {
    const matchEndIndex  = match.index + match[0].length;
    const followedByChar = text[matchEndIndex];

    if (punctuation.indexOf(followedByChar) < 0 && !isOptionalPluralAt(text, matchEndIndex))
      continue ;
    for (var ii = lastIndex ; ii < text.length && ii < match.index ; ++ii)
      newText += text[ii];
    if (match[0][match[0].length - 1] === 's' && text[match.index - 1] !== 's')
      newText += 's';
    lastIndex = match.index + match[0].length;
  }
  for (var ii = lastIndex ; ii < text.length ; ++ii)
    newText += text[ii];
  return newText;
}

function replaceInclusiveWording(text) {
  var newText = text;

  for (const word in dictionary) {
    const wordRegex = new RegExp('\\b' + word + '\\b', 'gi');

    newText = newText.replace(wordRegex, dictionary[word]);
  }
  return newText;
}

function replaceInclusiveWritingIn(node) {
  const modifiedText = replaceInclusiveWriting(node.textContent);
  const translatedText = replaceInclusiveWording(modifiedText);

  if (node.textContent !== translatedText)
    node.textContent = translatedText;
}

function exploreNode(node) {
  switch (node.nodeType) {
  case Node.TEXT_NODE:
    replaceInclusiveWritingIn(node);
    break ;
  case Node.ELEMENT_NODE:
    if (tagBlacklist.indexOf(node.tagName) < 0)
      node.childNodes.forEach(exploreNode);
    break ;
  }
}

function run() {
  console.info("Chromium Declusif Extension running...");
  exploreNode(document.body);
}

const mutationObserver = new MutationObserver(function (mutations) {
  mutations.forEach(function (mutation) {
    mutation.addedNodes.forEach(exploreNode);
  });
});

mutationObserver.observe(document.body, {characterData: true, childList: true, subtree: true});

run();
